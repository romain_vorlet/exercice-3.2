package fr.cnam.foad.nfa035.badges.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

/**
 * Commentez-moi
 */
public class DefaultBadgeCellRenderer extends DefaultTableCellRenderer {

    Color originForeground = null;

    /**
     * Commentez-moi
     */
    public DefaultBadgeCellRenderer() {
        super();
        this.originForeground = this.getForeground();
    }

    /**
     * Commentez-moi
     * @param table le composant JTable
     * @param value L'objet
     * @param isSelected Si sélectionné
     * @param hasFocus Si focus
     * @param row incice de ligne
     * @param column indice de colonne
     * @return Component
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,    row, column);

        if (((Date)table.getModel().getValueAt(row, 3)).before(new Date())){
            setForeground(Color.RED);
        }
        else{
            setForeground(this.originForeground);
        }

        return comp;
    }

}